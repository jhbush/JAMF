#!/bin/bash

###############################################################################
#
# Name: Uninstall_Parallels.sh
# Version: 1.0
# Create Date:  31 September 2015
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose:  Script for uninstalling the Parallels Mac Management Agent
#
###############################################################################

if [ -d /Library/Parallels/pma_agent.app/ ]; then
	/Library/Parallels/pma_agent.app/Contents/MacOS/pma_agent_uninstaller.app/Contents/Resources/UninstallAgentScript.sh
else
exit 0
fi
