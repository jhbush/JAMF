#!/bin/bash
###############################################################################
#
# Name: LocalAdmin.sh
# Version: 1.0
# Create Date:  15 June 2016
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script:
#				1. Prompts user for asset tag number (i.e. - 6100)
#				2. Ensures domain controller is up
#				3. Joins computer to AD domain through JAMF Pro policy
#		and remove any other domain accounts that are local admins
###############################################################################
# FUNCTIONS

# Function to ensure admin privileges
RunAsRoot() {
##  Pass in the full path to the executable as $1
if [[ "$(/usr/bin/id -u)" != "0" ]] ; then
echo "This application must be run with administrative privileges."
osascript -e "do shell script \"${1}\" with administrator privileges"
exit 0
fi
}

# Test if CocoaDialog is not installed. If not installed, it will be installed.
if [ ! -d "/path/to/CocoaDialog.app" ]; then
jamf policy -trigger installCocoa
fi
################################################################################
## User Variables

#Test if coputer is bound to AD
check4AD=`/usr/bin/dscl localhost -list . | grep "Active Directory"`

# Set the path to the cocoaDialog application.c
# Will be used to display prompts.
CD="/path/to/CocoaDialog.app/Contents/MacOS/CocoaDialog"

## More variables - No need to edit
rv=($($CD standard-inputbox --title "Computer Name" --no-cancel --float --no-newline --informative-text "Enter your Asset Tag Numer (e.g. - 6100)"))
ADCOMPUTERNAME='irbt-'${rv[1]}

DC={DomainController FQDN}
################################################################################

# Clear previous commands from Terminal
clear

# Execute runAsRoot function to ensure administrative privileges
RunAsRoot "${0}"

# Check for cocoaDialog dependency and exit if not found
if [[ ! -f "${CD}" ]]; then
echo "Required dependency not found: ${CD}"
exit 1
fi

## Change Computer Name with User input
scutil --set HostName $ADCOMPUTERNAME
sleep 1s
scutil --set LocalHostName $ADCOMPUTERNAME
sleep 1s
scutil --set ComputerName $ADCOMPUTERNAME
sleep 1s
dscacheutil -flushcache
sleep 5s

# If the machine is not bound to AD, then there's no purpose going any further.
if [[ "${check4AD}" != "Active Directory" ]]; then
if ping -c 2 -o $DC; then
jamf policy -id 13
else
rv=`$CD ok-msgbox --text "ERROR Binding Computer to AD!" \
--informative-text "(Please ensure the computer is connected to Secure Wifi or wired network)" \
--no-newline --float --no-cancel`
if [ "$rv" == "1" ]; then
	if ping -c 2 -o $DC; then
		jamf policy -id 13
	else
		rv=`$CD ok-msgbox --text "ERROR Binding Computer to AD!" \
		--informative-text "(Please ensure the computer is connected to Wifi or wired network)" \
		--no-newline --float --no-cancel`
		if [ "$rv" == "1" ]; then
			if ping -c 2 -o $DC; then
				jamf policy -id 13
			else	
				exit 1		
			fi
		fi	
	fi				
fi
fi
fi
