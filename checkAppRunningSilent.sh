#!/bin/bash -x

###############################################################################
#
# Name: closeAppRunningSilent.sh
# Version: 1.0
# Create Date:  01 February 2017
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script checks if specified applications are open and close the apps. 
#		   Script checks up to 5 apps and then runs a JAMF Pro custom triggered policy to install an app.
#	       
#
###############################################################################

# Path of Cocoadialog application
cdPath="/Library/iRobot/cocoaDialog.app/Contents/MacOS/cocoaDialog"

## Hardcoded values for testing
customTrigger="main_office"
app1="Microsoft Outlook"
app2="Microsoft Word"
app3="Microsoft Excel"
app4="Microsoft PowerPoint"
app5="Microsoft OneNote"

## Variable values from JAMF
if [[ "$customTrigger" == "" ]] && [[ "$4" != "" ]]; then
    customTrigger="$4"
elif [[ "$customTrigger" == "" ]] && [[ "$4" == "" ]]; then
    echo "A Custom trigger was not specified in parameter 4. Script cannot execute. Exiting..."
    exit 1
fi
if [[ "$app1" == "" ]] && [[ "$5" != "" ]]; then
    app1="$5"
elif [[ "$app1" == "" ]] && [[ "$5" == "" ]]; then
    echo "Application to Close 1 was not specified in parameter 5. Script cannot execute. Exiting..."
    exit 1
fi
if [[ "$app2" == "" ]] && [[ "$6" != "" ]]; then
    app2="$6"
elif [[ "$app2" == "" ]] && [[ "$6" == "" ]]; then
    echo "Application to Close 2 was not specified in parameter 6. Script cannot execute. Exiting..."
    exit 1
fi
if [[ "$app3" == "" ]] && [[ "$7" != "" ]]; then
    app3="$7"
elif [[ "$app3" == "" ]] && [[ "$7" == "" ]]; then
    echo "Application to Close 3 was not specified in parameter 7. Script cannot execute. Exiting..."
    exit 1
fi
if [[ "$app4" == "" ]] && [[ "$8" != "" ]]; then
    app4="$8"
elif [[ "$app4" == "" ]] && [[ "$8" == "" ]]; then
    echo "Application to Close 4 was not specified in parameter 8. Script cannot execute. Exiting..."
    exit 1
fi
if [[ "$app5" == "" ]] && [[ "$9" != "" ]]; then
    app5="$9"
elif [[ "$app5" == "" ]] && [[ "$9" == "" ]]; then
    echo "Application to Close 5 was not specified in parameter 9. Script cannot execute. Exiting..."
    exit 1
fi

## List of apps to check
appCheckList=(
"$app1"
"$app2"
"$app3"
"$app4"
"$app5"
)

## Function finds all running processes and checks against appCheckList
function checkForRunningApps ()
{

runningAppsList=()

x=0
while read appname; do
    if [[ $(ps axc | grep "$appname") != "" ]]; then
    	osascript -e 'quit app "'"$appname"'"'
        runningAppsList+=("${appCheckList[$x]}")
    fi
    let x=$((x+1))
done < <(printf '%s\n' "${appCheckList[@]}")


if [[ "${runningAppsList[@]}" != "" ]]; then
    if [[ $(ps axc | grep "${runningAppsList[$x]}") = "" ]]; then
    	## Call the install policy by manual trigger
    	echo "Error closing: "${runningAppsList[$x]}""
	else
    	echo "All running applications were shut down. Running JAMF policy "$customTrigger""
    	jamf policy -trigger "$customTrigger"
    fi	
else
    echo "No applications running to be shut down. Running JAMF policy "$customTrigger""
    jamf policy -trigger "$customTrigger"
fi

}

## Get the free disk space on the internal drive
freeDiskSpace=$(df -H / | awk '{getline; print $4}' | sed 's/[A-Z]//')

## If free space is too low, alert user and exit
if [[ "$freeDiskSpace" -lt 12 ]]; then
    exit 1
else
    ## Otherwise, move on to checking for running Office apps
    checkForRunningApps
fi