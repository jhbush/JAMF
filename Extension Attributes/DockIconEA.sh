#!/bin/bash

###############################################################################
#
# Name: DockIconEA.sh
# Version: 1.0
# Create Date:  01 February 2017
# Last Modified: 26 June 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script checks if specified applications is in the user's Dock. 
#	       
# Req's:   Dockutil.exe application deployed to each computer at location "/usr/local/dockutil"
###############################################################################


IFS=$'\n'
parent_dir=`/usr/bin/dirname "$0"`
dockutil_exe_path="/usr/local/dockutil/dockutil"
loggedInUser=`ls -l /dev/console | /usr/bin/awk '{ print $3 }'`

i="$(defaults read /Users/$loggedInUser/Library/Preferences/com.apple.dock persistent-apps | grep tile-type | awk '/file-tile/ {print NR}')"
    	# loop through the list of positions
    	for  j in `echo "$i"`
    	do
  		filePath=`/usr/libexec/PlistBuddy -c "Print persistent-apps:$[$j-1]:tile-data:file-data:_CFURLString" /Users/$loggedInUser/Library/Preferences/com.apple.dock.plist`		
		if [[ $filePath =~ "Self%20Service.app" ]]; then
        	selfservice=Yes
        fi
        done
        
    if [[ $selfservice = "Yes" ]]; then
    	echo "<result>Yes</result>"
    else
    	echo "<result>No</result>"
    fi
        


