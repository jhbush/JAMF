#!/bin/sh

###############################################################################
#
# Name: System Information (SystemInformation.sh)
# Version: 1.0
# Create Date:  31 January 2017
# Last Modified: 1 February 2017
#
# Author:  Adam Shuttleworth
# Purpose: Script to give end users system information on-screen, 
# allow them to take a screenshot and send all info to Service Desk email.
#
###############################################################################

JHELPER="/Library/Application Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper"

USERNAME=$(ls -l /dev/console | awk '{print $3}')
MACNAME=$(scutil --get ComputerName)
PORT=$(/usr/sbin/netstat -rn -f inet | awk '/default/{print $NF; exit}')
IPADDRESS=$(ipconfig getifaddr $PORT)
MACADDRESS=$(networksetup -getmacaddress $PORT | awk '{print $3}')

## Set the Support Email here if you want it hardcoded
SUPPORTEMAIL=""

## Check to see if the script was passed any script parameters from Casper
if [[ "$SUPPORTEMAIL" == "" ]] && [[ "$4" != "" ]]; then
	SUPPORTEMAIL="$4"
fi

INFO="You are logged in as:     $USERNAME
Computer name:          $MACNAME
IP Address:             $IPADDRESS
MAC Address:            $MACADDRESS"

THEMESSAGE=$("$JHELPER" -windowType utility -title "Your System Information" -description "$INFO" -button1 "OK" -button2 "Screenshot" -defaultButton 1 -icon "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericNetworkIcon.icns" -iconSize 64)

if [ "$THEMESSAGE" == "2" ]; then
DATE=$(date '+%Y-%m-%d_%I.%M%p')
if [[ ! -d "Users/Shared/ITSupport" ]];then 
	mkdir "/Users/Shared/ITSupport"
fi
FILENAME="Screen_Shot_${DATE}.png"
FILE="/Users/Shared/ITSupport/${FILENAME}"
screencapture -t png $FILE
#chown ${USERNAME}:staff $FILE  
fi

MESSAGE=$("$JHELPER" -windowType utility -title "Send Email to IT Support" -description "Which Mail Application do you use?" -button1 "Outlook" -button2 "Apple Mail" -defaultButton 1 -icon "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericNetworkIcon.icns" -iconSize 64)

HTML="<span style=font-family:'Arial';font-size:12.0pt>Hello iRobot IT Support,<br/><br/>Here is my diagnostic information:<br/><br/>
You are logged in as:     $USERNAME<br/>
Computer name:          $MACNAME<br/>
IP Address:             $IPADDRESS<br/>
MAC Address:            $MACADDRESS</span>"

TXT="Hello iRobot IT Support,

Here is my diagnostic information:

$INFO

"

## Create email in Outlook with Attachment
if [ "$MESSAGE" == "0" ] && [ "$THEMESSAGE" == "2" ]; then
osascript<<EOD
--Get Attachment
tell application "Finder"
set folderPath to folder "Macintosh HD:Users:Shared:ITSupport"
set theFile to first file in folderPath
set fileName to name of theFile
end tell

set theAttachment to theFile
set recipientName to "IT Service Desk"
set recipientAddress to "$SUPPORTEMAIL"
set theSubject to "System Information - $MACNAME"
set theContent to "$HTML"

--Send Email
tell application "Microsoft Outlook"
	activate
set theMessage to make new outgoing message with properties {subject:theSubject, content:theContent}
	##Set a recipient
make new recipient at theMessage with properties {email address:{name:recipientName, address:recipientAddress}}
	tell theMessage
		try
make new attachment with properties {file:theFile as alias}
set message_attachment to 0
on error errmess -- oops
log errmess -- log the error
set message_attachment to 1
end try
	open
end tell
end tell
EOD

ENDMESSAGE=$("$JHELPER" -windowType utility -title "Email Waiting to Be Sent" -description "Click Send on the email generated to send to $SUPPORTEMAIL." -button1 "OK" -defaultButton 1 -icon "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericNetworkIcon.icns" -iconSize 64)

## Create email in Outlook without Attachment
elif [ "$MESSAGE" == "0" ] && [ "$THEMESSAGE" == "0" ]; then
osascript<<EOD
set recipientName to "IT Service Desk"
set recipientAddress to "$SUPPORTEMAIL"
set theSubject to "System Information - $MACNAME"
set theContent to "$HTML"

--Send Email
tell application "Microsoft Outlook"
set theMessage to make new outgoing message with properties {subject:theSubject, content:theContent}
make new recipient at theMessage with properties {email address:{name:recipientName, address:recipientAddress}}
open theMessage
end tell
EOD

ENDMESSAGE=$("$JHELPER" -windowType utility -title "Email Waiting to Be Sent" -description "Click Send on the email generated to send to $SUPPORTEMAIL." -button1 "OK" -defaultButton 1 -icon "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericNetworkIcon.icns" -iconSize 64)

## Create email in Apple Mail with Attachment
elif [ "$MESSAGE" == "2" ] && [ "$THEMESSAGE" == "2" ]; then
osascript<<EOD
--Get Attachment
tell application "Finder"
set folderPath to folder "Macintosh HD:Users:Shared:ITSupport"
set theFile to first file in folderPath
set fileName to name of theFile
end tell

set theAttachment to theFile
set recipientAddress to "$SUPPORTEMAIL"
set theSubject to "System Information - $MACNAME"
set theContent to "$TXT"

tell application "Mail"
	activate
set theMessage to make new outgoing message with properties {subject:theSubject, content:theContent, visible:true}
	
	tell theMessage
	make new to recipient with properties {address:recipientAddress}
	try
make new attachment with properties {file name:theFile as alias} at after last paragraph
set message_attachment to 0
on error errmess -- oops
log errmess -- log the error
set message_attachment to 1
end try
		send
	end tell
end tell
EOD

ENDMESSAGE=$("$JHELPER" -windowType utility -title "System Information Email Sent" -description "System Information Email has been sent to $SUPPORTEMAIL." -button1 "OK" -defaultButton 1 -icon "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericNetworkIcon.icns" -iconSize 64)


## Create email in Apple Mail without Attachment
elif [ "$MESSAGE" == "2" ] && [ "$THEMESSAGE" == "0" ]; then
osascript<<EOD
set recipientAddress to "$SUPPORTEMAIL"
set theSubject to "System Information - $MACNAME"
set theContent to "$TXT"

tell application "Mail"
	activate
set theMessage to make new outgoing message with properties {subject:theSubject, content:theContent, visible:true}
	tell theMessage
	make new to recipient with properties {address:recipientAddress}
	end tell
	send theMessage
end tell
EOD

ENDMESSAGE=$("$JHELPER" -windowType utility -title "System Information Email Sent" -description "System Information Email has been sent to $SUPPORTEMAIL." -button1 "OK" -defaultButton 1 -icon "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/GenericNetworkIcon.icns" -iconSize 64)

fi

# Remove screenshot captured after sent via email.
if [[ -f $FILE ]]; then
	rm -R $FILE
	exit 0
else
	exit 0
fi