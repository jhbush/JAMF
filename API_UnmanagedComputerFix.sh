#!/bin/bash

###############################################################################
#
# Name: API_UnmanagedCompsFix.sh
# Version: 1.0
# Create Date:  01 February 2017
# Last Modified: 26 July 2017
#
# Author:  Adam Shuttleworth
# Purpose: This script:
#		1. Creates Advanced Search for Unmanaged Computers, if does not already exist
#		2. Creates Extension Attribute to note date of re-management, if does not alreay exist
#		3. Gets the list of unmanaged computers through a GET API call from the Advanced Computer Search created in Step 1
#		4. For each unmanaged computer found re-manages them through a PUT API call
#		5. Logs all activity to local log file
#
# Script can be run as a Jamf Pro Self Service policy for Helpdesk Techs. Setup as follows:
#	1. Add script to Jamf Pro and add Parameter labels for the 7 Global Variables
#	2. Create policy that runs the script with the proper parameters defined that is Self Service enabled
#	3. Scope policy to users that will run this script
#
###############################################################################

# Set Logging
LOGPATH='/var/log'
LOGFILE=$LOGPATH/APIUnmanagedCompsFix.log
STARTTIME=$( date "+%Y-%m-%d %H:%M" )

mkdir $LOGPATH
echo "$STARTTIME: Script started" > $LOGFILE

# Global Variables, if hardcoded
apiUser="" # API user needs RW access to Computer Objects, Advanced Computer Searches, and Extension Attributes 	
apiPass=""		
jamfURL="" 
management_username=""
management_password=""
EAName="" ## Please use no special other name spaces. Those will be normalized for the API call below.
SearchName="" ## Please use no special other name spaces. Those will be normalized for the API call below.

# Check to see if the script was passed any script parameters from JAMF Pro
if [[ "$apiUser" == "" ]] && [[ "$4" != "" ]]; then
	apiUser="$4"
fi

if [[ "$apiPass" == "" ]] && [[ "$5" != "" ]]; then
	apiPass="$5"
fi

if [[ "$jamfURL" == "" ]] && [[ "$6" != "" ]]; then
	jamfURL="$6"
fi

if [[ "$management_username" == "" ]] && [[ "$7" != "" ]]; then
	management_username="$7"
fi

if [[ "$management_password" == "" ]] && [[ "$8" != "" ]]; then
	management_password="$8"
fi

if [[ "$EAName" == "" ]] && [[ "$9" != "" ]]; then
	EAName="$9"
fi

if [[ "$SearchName" == "" ]] && [[ "$10" != "" ]]; then
	SearchName="$10"
fi

## Finally, make sure we got at least an apiUser & apiPass variable, else we exit
if [[ -z "$apiUser" ]] || [[ -z "$apiPass" ]]; then
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: One of the required variables was not passed to the script. Exiting..." >> $LOGFILE
	exit 1
fi

## If no server address was passed to the script, get it from the Mac's com.jamfsoftware.jamf.plist
if [[ -z "$jamfURL" ]]; then
	jamfURL=$( /usr/bin/defaults read /Library/Preferences/com.jamfsoftware.jamf.plist jss_url 2> /dev/null | sed 's/\/$//' )
	if [[ -z "$jamfURL" ]]; then
		DATE=$( date "+%Y-%m-%d %H:%M" )
		echo "$DATE: Jamf URL = $jamfURL" >> $LOGFILE
		DATE=$( date "+%Y-%m-%d %H:%M" )
		echo "$DATE: Oops! We couldn't get the Jamf URL from this Mac, and none was passed to the script" >> $LOGFILE
		exit 1
	else
		DATE=$( date "+%Y-%m-%d %H:%M" )
		echo "$DATE: Jamf URL = $jamfURL" >> $LOGFILE
	fi
else
	## Make sure to remove any trailing / in the passed parameter for the JAMF Pro URL
	jamfURL=$( echo "$jamfURL" | sed 's/\/$//' )
fi

## SCRIPT
# Get {SearchName} Advanced Computer Search ID
SearchNameNormalized=$( echo "$SearchName" | sed -e 's/ /%20/g' ) ## Make sure to replace spaces in URL to %20
SearchURL="${jamfURL}/JSSResource/advancedcomputersearches/name/${SearchNameNormalized}"
Search=$( curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${SearchURL}" )

# Check if $Search has html in it. This identifies that the EA does not exist. Create if does not exist.
if [[ $Search == *html* ]]; then
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: New Advanced Computer Search being created." >> $LOGFILE

	# Create Advanced Computer Search
	newSearchURL="${jamfURL}/JSSResource/advancedcomputersearches/id/0"
	curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${newSearchURL}" -d "<advanced_computer_search>
		<id>0</id>
		<name>$SearchName</name>
		<view_as>Standard Web Page</view_as>
		<sort_1/>
		<sort_2/>
		<sort_3/>
		<criteria>
			<size>1</size>
			<criterion>
				<name>Managed</name>
				<priority>0</priority>
				<and_or>and</and_or>
				<search_type>is</search_type>
				<value>Unmanaged</value>
				<opening_paren>false</opening_paren>
				<closing_paren>false</closing_paren>
			</criterion>
		</criteria>
		<display_fields>
			<size>18</size>
			<display_field>
				<name>Computer Name</name>
			</display_field>
			<display_field>
				<name>Last Check-in</name>
			</display_field>
			<display_field>
				<name>Last Enrollment</name>
			</display_field>
			<display_field>
				<name>Last Reported IP Address</name>
			</display_field>
			<display_field>
				<name>Managed</name>
			</display_field>
			<display_field>
				<name>Managed By</name>
			</display_field>
			<display_field>
				<name>Model Identifier</name>
			</display_field>
			<display_field>
				<name>Serial Number</name>
			</display_field>
			<display_field>
				<name>Operating System Version</name>
			</display_field>
			<display_field>
				<name>Building</name>
			</display_field>
			<display_field>
				<name>Department</name>
			</display_field>
			<display_field>
				<name>Email Address</name>
			</display_field>
			<display_field>
				<name>Position</name>
			</display_field>
			<display_field>
				<name>Username</name>
			</display_field>
			<display_field>
				<name>IP Geo-Location</name>
			</display_field>
			<display_field>
				<name>Last User</name>
			</display_field>
			<display_field>
				<name>Network Services Installed</name>
			</display_field>
			<display_field>
				<name>VPN Profiles Installed</name>
			</display_field>
		</display_fields>
		<computers>
			<size>0</size>
		</computers>
		<site>
			<id>-1</id>
			<name>None</name>
		</site>
	</advanced_computer_search>" -X POST
	
	# Sleep for 15 seconds to allow EA to be fully posted to database
	sleep 15
else
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: Advanced Computer already exists." >> $LOGFILE
fi

# Get {EAName} EA ID
EANameNormalized=$( echo "$EAName" | sed -e 's/ /%20/g' ) ## Make sure to replace spaces in URL to %20
eaURL="${jamfURL}/JSSResource/computerextensionattributes/name/${EANameNormalized}"
EA=$( curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${eaURL}" )

# Check if $EA has html in it. This identifies that the EA does not exist. Create if does not exist.
if [[ $EA == *html* ]]; then
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: New Extension Attribute being created." >> $LOGFILE
	# Create Extension Attribute "Unmanaged Fixed" through API
	newEAURL="${jamfURL}/JSSResource/computerextensionattributes/id/0"
	curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${newEAURL}" -d "<computer_extension_attribute>
		<name>$EAName</name>
		<description>This EA lists if a previous managed computer turns to unmanaged and has been fixed with a script.</description>
		<data_type>Date</data_type>
		<input_type>
			<type>Text Field</type>
		</input_type>
		<inventory_display>Extension Attributes</inventory_display>
		<recon_display>Extension Attributes</recon_display>
	</computer_extension_attribute>" -X POST
	
	# Sleep for 15 seconds to allow EA to be fully posted to database
	sleep 15
	
else
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: Extension Attribute already exists." >> $LOGFILE
fi

# Create xls file to be used with xsltproc
XLSFile="/var/tmp/ManagementSettings.xls"
echo '<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
<xsl:template match="/advanced_computer_search"> 	 	
	<xsl:for-each select="computers/computer">
		<xsl:value-of select="id"/>
		<xsl:text>&#xa;</xsl:text>
	</xsl:for-each>
</xsl:template> 
</xsl:stylesheet>' > $XLSFile

# Get all Computer records from "Unmanaged Computers" Advanced Computer Search
compRecords=$( curl -ksH "Accept: application/xml" -u "${apiUser}":"${apiPass}" "${SearchURL}" | xsltproc "${XLSFile}" -)

# Parse computer records for those not managed
if [[ -z $compRecords ]]; then
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: All computers are managed...:)" >> $LOGFILE
else
	DATE=$( date "+%Y-%m-%d %H:%M" )
	echo "$DATE: There are unmanaged computers...:(" >> $LOGFILE
	for i in $compRecords; do
		URL="${jamfURL}/JSSResource/computers/id/${i}"
		date=$( date +"%F %T" )
		# Get Computer Name for logging
		compName=$( curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${URL}" | xmllint --xpath "/computer/general/name/text()" - )
		DATE=$( date "+%Y-%m-%d %H:%M" )
		echo "$DATE: Computer ${compName} is being re-managed" >> $LOGFILE
		## Get computer object through JAMF Pro API and username associated with computer object 
		curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${URL}" -d "<computer>
				<general>
					<remote_management>
						<managed>true</managed>
						<management_username>$management_username</management_username>
						<management_password>$management_password</management_password>
					</remote_management>
				</general>
			</computer>" -X PUT
		sleep 10
		curl -ksH "Content-Type: application/xml" -u "${apiUser}":"${apiPass}" "${URL}" -d "<computer>
		<extension_attributes> <attribute> <name>$EAName</name> <value>$date</value> </attribute>
		</extension_attributes>
		</computer>" -X PUT
	done
fi
rm $XLSFile
ENDTIME=$( date "+%Y-%m-%d %H:%M" )
echo "$ENDTIME: Script completed" >> $LOGFILE
echo "" >> $LOGFILE

open $LOGFILE