#!/bin/sh -x
#
#
# Script provided by Switch Belgium. http://www.switch.be
#
#

sudo rm /var/db/.AppleSetupDone
sudo rm -rf /var/db/ConfigurationProfiles/
sudo rm /Library/Keychains/apsd.keychain

# Get a list from all profiles installed on the computer and remove every one of them

for identifier in $(profiles -L | awk "/attribute/" | awk '{print $4}')
do profiles -R -p $identifier
done

# Delete all users and homefolders and reboot

realUsrs=()
realUsrHomeDirs=()
for usr in $(dscl . -list /Users); do 
  if [[ $usr != 'Guest' ]]; then # Exclude the 'Guest' user.
    # We identify real users by whether their home directory paths start with '/Users/'
    homeDir=$(dscl . -read /Users/$usr NFSHomeDirectory | awk '{print $2}') || die
    [[ $homeDir == /Users/* ]] && { realUsrs+=($usr); realUsrHomeDirs+=( "$homeDir" ); }
  fi
done

(( ${#realUsrs[@]} > 0 )) || die "Failed to determine usernames of regular users."

i=0
for usr in "${realUsrs[@]}"; do

  homeDir=${realUsrHomeDirs[i++]}

  echo "  Deleting user '$usr' and her files..."

  # Delete user's Directory Services group memberships.
  for grp in $(dscl . -search /groups GroupMembership $usr | awk 'NF>1 {print $1}'); do
    dscl . -delete /Groups/$grp GroupMembership $usr || die
  done

  # Delete user itself.
  dscl . -delete /users/$usr || die

  # Delete user's home folder.
  rm -rf "$homeDir" || die

done
# rm -rf /Applications/Teamviewer
jamf removeFramework
rm -rf /var/log/jamf.log
shutdown -r now